package com.hpc.ps.model;

/**
 * Enum containing the two types of densities - hares and pumas.
 *
 * @author Nathan Giles-Donovan
 * Created on 10/10/2016.
 */
public enum DensityType {

  /**
   * Values representing density of hares.
   */
  HARES,

  /**
   * Values representing density of pumas.
   */
  PUMAS;

}
