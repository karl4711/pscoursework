package com.hpc.ps.model;

/**
 * Representation of a landscape. This is stored as a lattice of sites which can either be {@link #LAND}
 * or {@link #WATER}.
 *
 * @author Nathan Giles-Donovan
 * Created on 10/10/2016.
 */
public class Landscape {

  private final int[][] landscape;
  private int[][] numberOfNeighboursNotWater;

  private int numberOfSiteNotWater;
  private final int numberOfRows;
  private final int numberOfCols;

  private boolean needToMakeNearestNeighboursMatrix, needToRecalculateNumberOfWaterSites;

  /**
   * Value representing a water tile.
   */
  public static final int WATER = 0;

  /**
   * Value representing a land tile.
   */
  public static final int LAND = 1;

  /**
   * Constructs a new landscape of the given size (numberOfRows x numberOfColumns).
   *
   * @param numberOfRows    The number of rows in this landscape
   * @param numberOfCols    The number of columns in this landscape
   */
  public Landscape(int numberOfRows, int numberOfCols) {
    this.numberOfRows = numberOfRows;
    this.numberOfCols = numberOfCols;
    landscape = new int[numberOfRows][numberOfCols];
    needToMakeNearestNeighboursMatrix = true;
    needToRecalculateNumberOfWaterSites = true;
  }

  /**
   * Returns the numerical value of a site positioned with indices <code>row, column</code>.
   * <p>
   * The returned value can be assigned meaning by cross-referencing with {@link #LAND} and
   * {@link #WATER}.
   *
   * @param row       The row of the desired site
   * @param column    The column of the desired site
   *
   * @return          The value of the landscape at the desired position.
   */
  public int getLandscapeElement(int row, int column) {
    return landscape[row][column];
  }

  /**
   * Sets the value of the landscape at a site with indices <code>row, column</code>.
   * <p>
   * The value being set is recommended to be one of: {@link #LAND}, {@link #WATER}.
   *
   * @param row       The row of the desired site
   * @param column    The column of the desired site
   * @param value     The value to be set
   */
  public void setLandscapeElement(int row, int column, int value) {
    landscape[row][column] = value;
    needToMakeNearestNeighboursMatrix = true;
    needToRecalculateNumberOfWaterSites = true;
  }

  /**
   * Gets the number of rows of the landscape.
   *
   * @return          The number of rows of the landscape.
   */
  public int getNumberOfRows() {
    return numberOfRows;
  }

  /**
   * Gets the number of columns of the landscape.
   *
   * @return          The number of columns of the landscape.
   */
  public int getNumberOfCols() {
    return numberOfCols;
  }

  /**
   * Returns the number of sites in the landscape that are not set to {@link #WATER}.
   * <p>
   * Implementation of this method is such that the full calculation is only performed on the first
   * call or if the landscape is changed in any way using {@link #setLandscapeElement(int, int, int)}.
   *
   * @return        The number of sites not occupied by {@link #WATER}.
   */
  public int getNumberOfSiteNotWater() {
    if (needToRecalculateNumberOfWaterSites) {
      calcNumberOfSitesNotWater();
      needToRecalculateNumberOfWaterSites = false;
    }
    return numberOfSiteNotWater;
  }

  /* Calculates the total number of sites not occupied by water. As the only values are 1(land) and
   * 0(water), a sum is just performed over all the sites.
   */
  private void calcNumberOfSitesNotWater() {
    numberOfSiteNotWater = 0;
    for (int i = 0; i < landscape.length; i++) {
      for (int j = 0; j < landscape[i].length; j++) {
        numberOfSiteNotWater += landscape[i][j];
      }
    }
  }

  /**
   * Returns the number of neighbours of the site with indices <code>row, col</code> that are not
   * set to {@link #WATER}.
   * <p>
   * Implementation of this method is such that the full calculation is only performed on the first
   * call or if the landscape is changed in any way using {@link #setLandscapeElement(int, int, int)}.
   *
   * @return        The number of neighbouring sites not occupied by {@link #WATER}.
   */
  public int getNumberOfNeighboursNotWater(int row, int col) {
    if(needToMakeNearestNeighboursMatrix){
      calcNumberOfNeighboursNotWater();
      needToMakeNearestNeighboursMatrix = false;
    }
    return numberOfNeighboursNotWater[row][col];
  }

  /* Populates the 'numberOfNeighboursNotWater' matrix.
   * Calculates the total number of neighbouring sites not occupied by water for each site. As the
   * only values are 1(land) and 0(water), a sum is just performed over all the neighbours.
   *
   * Uses 'getElementWithBoundaryCondition' instead of 'getElement' to account for the boundary conditions
   * (set edges to water - ie 0 - at the edges).
   */
  private void calcNumberOfNeighboursNotWater() {
    numberOfNeighboursNotWater = new int[numberOfRows][numberOfCols];
    for (int i = 0; i < numberOfRows; i++) {
      for (int j = 0; j < numberOfCols; j++) {
        numberOfNeighboursNotWater[i][j] = getElementWithBoundaryConditions(i - 1,j) + getElementWithBoundaryConditions(i + 1,j)
          + getElementWithBoundaryConditions(i,j - 1) + getElementWithBoundaryConditions(i,j + 1);
      }
    }
  }

  /* Wraps 'getElement' to account for cases where 'row' or 'column' are outside the valid ranges.
   * In this case 0 is just returned in keeping with the boundary conditions of edges set to water
   * - ie 0.
   */
  private int getElementWithBoundaryConditions(int row, int column){
    if(row < 0 | row >= numberOfRows | column < 0 | column >= numberOfCols){
      return 0;
    }
    return getLandscapeElement(row, column);
  }

}
