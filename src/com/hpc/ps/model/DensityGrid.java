package com.hpc.ps.model;

/**
 * DensityGrid Interface
 *
 * @author Jiangsu Du, Nathan Giles-Donovan Created on 10/10/2016.
 */
public interface DensityGrid {

    /**
     * get the number of columns
     *
     * @return the number of columns
     */
    int getColumnsNumber();

    /**
     * get the number of rows
     *
     * @return the number of rows
     */
    int getRowsNumber();


    /**
     * get the value at the specified position in the grid, return 0 if not accessible
     *
     * @param row    specified row
     * @param column specified column
     * @return the value at the specified position, 0 if not accessible
     */
    double get(int row, int column);

    /**
     * set value at the specified position in the grid
     *
     * @param row    specified row
     * @param column specified column
     * @param value  value to set
     */
    void set(int row, int column, double value);

}
