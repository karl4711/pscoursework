package com.hpc.ps.model;

import com.hpc.ps.Main;
import com.hpc.ps.io.Output;
import com.hpc.ps.io.OutputImpl;
import com.hpc.ps.model.solver.Solver;
import com.hpc.ps.model.solver.SolverParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Model to simulate two populations of hares and pumas living on a landscape.
 *
 * For more information see {@link <a href="../../../../../../DevelopmentCoursework.pdf">Development Requirements</a>}.
 *
 * @author Bowen Sun, Nathan Giles-Donovan
 */
// Created on 08/10/2016
public class PreyModel {

  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  // Landscape
  private final Landscape landscape;

  // Density of hares
  private DensityGrid hareDensity;

  // Density of pumas
  private DensityGrid pumaDensity;

  // Solver used to solve the equation
  private final Solver solver;

  // The time between successive times
  private final double timeStepSize;

  // The number of updates between successive outputs
  private final int numberOfTimeStepsBetweenOutput;

  // The maximum time the simulation will run for
  private final double timeMax;

  private final String outputPath;

  /**
   * Constructs a model with the given parameters.
   *
   * @param landscape                      The landscape the model is to run on.
   * @param hareDensity                    An initial density of hares.
   * @param pumaDensity                    An initial density of pumas.
   * @param solverParams                   The parameters to be passed to the solver.
   * @param timeStepSize                   The time between successive iterations of the
   *                                       model.
   * @param numberOfTimeStepsBetweenOutput The number of iterations between successive file
   *                                       outputs.
   * @param timeMax                        The maximum value of time the simulation may run for.
   * @param outputPath                     The path used by the {@link com.hpc.ps.io.Output}.
   */
  public PreyModel(Landscape landscape, DensityGrid hareDensity,
                   DensityGrid pumaDensity, SolverParameters solverParams,
                   double timeStepSize, int numberOfTimeStepsBetweenOutput,
                   double timeMax, String outputPath) {

    this.landscape = landscape;
    this.hareDensity = hareDensity;
    this.pumaDensity = pumaDensity;
    this.timeStepSize = timeStepSize;
    this.numberOfTimeStepsBetweenOutput = numberOfTimeStepsBetweenOutput;
    this.timeMax = timeMax;
    this.outputPath = outputPath;

    solver = new Solver(solverParams, timeStepSize);
  }

  /**
   * Starts the simulation running.
   *
   * @throws IOException Exception thrown if an IO issue was encountered during the running of the
   *                     model (in the outputting of the files).
   */
  public void start() throws IOException {
    run();
  }

  /**
   * Runs the model. Runs till the time exceeds 'maxTime'.
   *
   * Outputs to the output files at intervals specified by
   * 'numberOfTimeStepsBetweenOutput'. The image file containing the densities
   * is written whilst the program is running. The list of average densities
   * is written at the end
   *
   * Every time step new density grids are made which are then filled with the
   * 'new' densities. The old grid is then overwritten by these new ones.
   *
   * Water tiles are not updated.
   */
  private void run() throws IOException {
    double time = 0;
    int outputCounter = 0;
    Output output = new OutputImpl(outputPath);
    List<Double> hareAverageList = new ArrayList<>();
    List<Double> pumaAverageList = new ArrayList<>();

    // Initial output
    outputToFile(output, hareAverageList, pumaAverageList, time);

    while (time < timeMax) {

      // Output
      if (outputCounter == numberOfTimeStepsBetweenOutput) {
        outputToFile(output, hareAverageList, pumaAverageList, time);
        outputCounter = 0;
      } else {
        outputCounter++;
      }


      DensityGrid newHareDensity = new DensityGridImpl(
        landscape.getNumberOfRows(), landscape.getNumberOfCols());
      DensityGrid newPumaDensity = new DensityGridImpl(
        landscape.getNumberOfRows(), landscape.getNumberOfCols());
      for (int i = 0; i < landscape.getNumberOfRows(); i++) {
        for (int j = 0; j < landscape.getNumberOfCols(); j++) {
          if (landscape.getLandscapeElement(i, j) != Landscape.WATER) {
            int numberOfLandNeighbours = landscape
              .getNumberOfNeighboursNotWater(i, j);
            double newHareValue = solver.updateDensity(
              DensityType.HARES, i, j, hareDensity,
              pumaDensity, numberOfLandNeighbours);
            newHareDensity.set(i, j, newHareValue);
            double newPumaValue = solver.updateDensity(
              DensityType.PUMAS, i, j, hareDensity,
              pumaDensity, numberOfLandNeighbours);
            newPumaDensity.set(i, j, newPumaValue);
          }
        }
      }
      hareDensity = newHareDensity;
      pumaDensity = newPumaDensity;
      time += timeStepSize;
    }

    // Output the densities to a file.
    output.writeAverages(hareAverageList, pumaAverageList);
  }

  //Outputs the ppm file and adds the current averages to the list for outputting later.
  private void outputToFile(Output output, List<Double> hareAverageList, List<Double> pumaAverageList, double time) {
    output.writePPM(hareDensity, pumaDensity, landscape);
    hareAverageList.add(calcAverageHareDensity());
    pumaAverageList.add(calcAveragePumaDensity());

    logger.info(String.format("Files outputted at simulation time %.3f", time));
  }

  /**
   * Averages the density of hares over the non-water sites.
   *
   * @return The average density of hares.
   */
  public double calcAverageHareDensity() {
    double sum = 0;
    for (int i = 0; i < landscape.getNumberOfRows(); i++) {
      for (int j = 0; j < landscape.getNumberOfCols(); j++) {
        sum += hareDensity.get(i, j);
      }
    }
    return sum / landscape.getNumberOfSiteNotWater();
  }

  /**
   * Averages the density of pumas over the non-water sites.
   *
   * @return The average density of pumas.
   */
  public double calcAveragePumaDensity() {
    double sum = 0;
    for (int i = 0; i < landscape.getNumberOfRows(); i++) {
      for (int j = 0; j < landscape.getNumberOfCols(); j++) {
        sum += pumaDensity.get(i, j);
      }
    }
    return sum / landscape.getNumberOfSiteNotWater();
  }

}
