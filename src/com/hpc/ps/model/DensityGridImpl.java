package com.hpc.ps.model;

import java.util.Random;

/**
 * Density Grid implementation
 *
 * @author Jiangsu Du, Nathan Giles-Donovan Created on 10/10/2016.
 */
public class DensityGridImpl implements DensityGrid {

	private double densityGridArray[][];

	public DensityGridImpl(Landscape landscape, Random rand, double maxValue) {
		int noRows = landscape.getNumberOfRows();
		int noCols = landscape.getNumberOfCols();
		densityGridArray = new double[noRows][noCols];
		for (int i = 0; i < noRows; i++) {
			for (int j = 0; j < noCols; j++) {
				if (landscape.getLandscapeElement(i, j) != Landscape.WATER) {
					densityGridArray[i][j] = rand.nextDouble() * maxValue;
				}
			}
		}
	}

    public DensityGridImpl(int noRows, int noCols) {
        densityGridArray = new double[noRows][noCols];
    }

	@Override
	public double get(int row, int column) {
		if (row < 0 | row >= densityGridArray.length | column < 0
				| column >= densityGridArray[0].length) {
			return 0;
		}
		return this.densityGridArray[row][column];
	}

	@Override
	public void set(int row, int column, double value) {
		this.densityGridArray[row][column] = value;
	}

	@Override
	public int getColumnsNumber() {
		return this.densityGridArray[0].length;
	}

	@Override
	public int getRowsNumber() {
		return this.densityGridArray.length;
	}
}
