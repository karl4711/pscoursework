package com.hpc.ps.model.solver;

import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.DensityType;

/**
 * Used by the program to perform the update of the density of the pumas and hares via an implementation
 * of the algorithm outlined in the design brief ({@link "DevelopmentCoursework.pdf"}).
 *
 * @author Nathan Giles-Donovan
 * Created on 10/10/2016.
 */
public class Solver {

  //birth rate of hares
  private final double r;

  //predation rate
  private final double a;

  //birth rate of pumas per one hare eaten
  private final double b;

  //puma mortality rate
  private final double m;

  //diffusion rates for hares
  private final double k;

  //diffusion rates for pumas
  private final double l;

  //time step
  private final double timeStepSize;

  /**
   * Constructs a solver with the given solver parameters and time step.
   *
   * @param solverParameters    The parameters to be used in the solving of the equations
   * @param timeStepSize        The time between successive iterations of the model
   */
  public Solver(SolverParameters solverParameters, double timeStepSize) {
    this.r = solverParameters.getR();
    this.a = solverParameters.getA();
    this.b = solverParameters.getB();
    this.m = solverParameters.getM();
    this.k = solverParameters.getK();
    this.l = solverParameters.getL();
    this.timeStepSize = timeStepSize;
  }

  /**
   * Performs an update of the given density according to the equations.
   * <p>
   * The new value of the density is forced to be positive. This is done by explicitly setting
   * negative values to 0.
   *
   *
   * @param densityType             The type of density to update. This specifies which equation is used
   * @param row                     The row index of the site to be updated
   * @param col                     The column index of the site to be updated
   * @param oldHareDensity          The {@link DensityGrid}containing the old density distribution of the hares
   * @param oldPumaDensity          The {@link DensityGrid}containing the old density distribution of the pumas
   * @param numberOfLandNeighbours  The number of 'dry' (i.e non water) sites that neighbour the site that is being updated
   *
   * @return                        The (positive) updated value of the density.
   */
  public double updateDensity(DensityType densityType, int row, int col, DensityGrid oldHareDensity,
                              DensityGrid oldPumaDensity, int numberOfLandNeighbours) {
    double haresOld = oldHareDensity.get(row, col);
    double pumasOld = oldPumaDensity.get(row, col);
    double newVal = 0;
    if (densityType == DensityType.HARES) {
      double birthTerm = r * haresOld;
      double deathTerm = a * haresOld * pumasOld;
      double diffusionTerm = oldHareDensity.get(row - 1, col) + oldHareDensity.get(row + 1, col)
        + oldHareDensity.get(row, col - 1) + oldHareDensity.get(row, col + 1)
        - numberOfLandNeighbours * haresOld;
      double change = timeStepSize * (birthTerm - deathTerm + k * diffusionTerm);
      newVal = haresOld + change;
    } else if (densityType == DensityType.PUMAS) {
      double birthTerm = b * haresOld * pumasOld;
      double deathTerm = m * pumasOld;
      double diffusionTerm = oldPumaDensity.get(row - 1, col) + oldPumaDensity.get(row + 1, col)
        + oldPumaDensity.get(row, col - 1) + oldPumaDensity.get(row, col + 1)
        - numberOfLandNeighbours * pumasOld;
      double change = timeStepSize * (birthTerm - deathTerm + l * diffusionTerm);
      newVal = pumasOld + change;
    }
    if(newVal < 0){
      return 0;
    }
    return newVal;
  }

}

