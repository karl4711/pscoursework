package com.hpc.ps.model.solver;

/**
 * Used to hold all the parameters from the equations used by the solver.
 *
 * @author Nathan Giles-Donovan
 */
//Created on 10/10/2016.
public class SolverParameters {

  private final double r, a, b, m, k, l;

  /**
   * Constructs a <code>SolverParameters</code> object that contains the given collection of parameters
   * to be used by the solver.
   *
   * For more information about the naming convention used see {@link <a href="../../../../../../DevelopmentCoursework.pdf">Development Requirements</a>}
   *
   * @param r         The value of the birth rate of hares
   * @param a         The value of the predation rate of hares
   * @param b         The birth rate of pumas per hare eaten
   * @param m         The mortality rate of pumas
   * @param k         The diffusion rate of hares
   * @param l         The diffusion rate of pumas
   */
  public SolverParameters(double r, double a, double b, double m, double k, double l) {
    this.r = r;
    this.a = a;
    this.b = b;
    this.m = m;
    this.k = k;
    this.l = l;
  }

  /**
   * Gets the value of the birth rate of hares (called r in the documentation).
   *
   * @see <a href="../../../../../../DevelopmentCoursework.pdf">Development Requirements</a>
   *
   * @return  The value of r.
   */
  public double getR() {
    return r;
  }

  /**
   * Gets the value of the predation rate of hares(called a in the documentation).
   * This is the rate of hares eaten per puma.
   *
   * @see <a href="../../../../../../DevelopmentCoursework.pdf">Development Requirements</a>
   *
   * @return  The value of a.
   */
  public double getA() {
    return a;
  }

  /**
   * Gets the value of the birth rate of pumas per hare eaten (called b in the documentation).
   *
   * @see <a href="../../../../../../DevelopmentCoursework.pdf">Development Requirements</a>
   *
   * @return  The value of b.
   */
  public double getB() {
    return b;
  }

  /**
   * Gets the value of the mortality rate of pumas (called m in the documentation).
   *
   * @see <a href="../../../../../../DevelopmentCoursework.pdf">Development Requirements</a>
   *
   * @return  The value of m.
   */
  public double getM() {
    return m;
  }

  /**
   * Gets the value of the diffusion rate of hares (called k in the documentation).
   *
   * @see <a href="../../../../../../DevelopmentCoursework.pdf">Development Requirements</a>
   *
   * @return  The value of k.
   */
  public double getK() {
    return k;
  }

  /**
   * Gets the value of the diffusion rate of pumas (called l in the documentation).
   *
   * @see <a href="../../../../../../DevelopmentCoursework.pdf">Development Requirements</a>
   *
   * @return  The value of l.
   */
  public double getL() {
    return l;
  }
}
