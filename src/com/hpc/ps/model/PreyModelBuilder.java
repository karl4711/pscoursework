package com.hpc.ps.model;

import com.beust.jcommander.Parameter;
import com.hpc.ps.io.Input;
import com.hpc.ps.io.InputImpl;
import com.hpc.ps.model.solver.SolverParameters;
import com.hpc.ps.validator.FileValidator;
import com.hpc.ps.validator.PositiveValidator;

import java.io.IOException;
import java.util.Random;

/**
 * Builder for the prey model.
 *
 * @author Bowen Sun, Nathan Giles-Donovan
 */
//Created on 08/10/2016.
public class PreyModelBuilder {

  private Landscape landscape;
  private DensityGrid h;
  private DensityGrid p;

  public boolean help() {
    return help;
  }

  @Parameter(names = "-help", description = "Displays a list of all parameters and then exits.", help = true)
  private boolean help = false;

  @Parameter(names = {"-f"}, description = "parameter config file path", validateWith = FileValidator.class)
  private String parameterFilePath;

  @Parameter(names = {"-landscape"}, description = "file path of the landscape", validateWith = FileValidator.class)
  private String landscapeFilePath = "files/islands.dat";

  @Parameter(names = {"-hareDensity"}, description = "file path of initial hare density grid", validateWith = FileValidator.class)
  private String hareDensityFilePath;

  @Parameter(names = {"-pumaDensity"}, description = "file path of initial puma density grid", validateWith = FileValidator.class)
  private String pumaDensityFilePath;

  @Parameter(names = {"-r"}, description = "birth rate of hares", validateWith = PositiveValidator.class)
  private double r = 0.08;

  @Parameter(names = {"-a"}, description = "predation rate", validateWith = PositiveValidator.class)
  private double a = 0.04;

  @Parameter(names = {"-b"}, description = "birth rate of pumas per one hare eaten", validateWith = PositiveValidator.class)
  private double b = 0.02;

  @Parameter(names = {"-m"}, description = "puma mortality rate", validateWith = PositiveValidator.class)
  private double m = 0.06;

  @Parameter(names = {"-k"}, description = "diffusion rates for hares", validateWith = PositiveValidator.class)
  private double k = 0.2;

  @Parameter(names = {"-l"}, description = "diffusion rates for pumas", validateWith = PositiveValidator.class)
  private double l = 0.2;

  @Parameter(names = {"-t"}, description = "size of time step", validateWith = PositiveValidator.class)
  private double timeStepSize = 0.4;

  @Parameter(names = {"-T"}, description = "number of time steps between output", validateWith = PositiveValidator.class)
  private int numberOfTimeStepsBetweenOutput = 10;

  @Parameter(names = {"-seed"}, description = "seed for the random number generator used for random initial conditions", validateWith = PositiveValidator.class)
  private long seed = 0;

  @Parameter(names = {"-maxTime"}, description = "The maximum amount of time that the program will run for", validateWith = PositiveValidator.class)
  private double maxTime = 500;

  //The maximum value of any site in the random initial configuration (i.e. the initial grid will be in the range (0,maxValue)).
  private double maxValue = 5;

  //The path used for the outputted files
  private String outputPath = "./files";

  /**
   * Build the prey model with specified parameters
   *
   * @return the prey model
   * @throws IOException if reading files failed
   */
  public PreyModel buildModel() throws IOException {

    Input input = new InputImpl();
    landscape(input.readLandscape(landscapeFilePath));
    //initialize density through file
    if (hareDensityFilePath != null && pumaDensityFilePath != null) {
      this.h(input.readDensityGrid(hareDensityFilePath)).p(
        input.readDensityGrid(pumaDensityFilePath));
    } 
    //if no density file, code will initialize density by using specified random sequence
    else {
      Random rand;
      if (seed == 0) {
        rand = new Random();
      } else {
        rand = new Random(seed);
      }
      h = new DensityGridImpl(landscape, rand, maxValue);
      p = new DensityGridImpl(landscape, rand, maxValue);
    }
    //initialize parameters
    SolverParameters solverParameters = new SolverParameters(r, a, b, m, k,
      l);
    return new PreyModel(landscape, h, p, solverParameters, timeStepSize,
      numberOfTimeStepsBetweenOutput, maxTime, outputPath);
  }

  /**
   * Gets the file path of the parameter file.
   *
   * @return The file path of the parameter
   */
  public String getParameterFilePath() {
    return parameterFilePath;
  }

  /**
   * Sets the file path of the landscape file.
   *
   * @param landscapeFilePath The file path.
   * @return                  The builder object
   */
  public PreyModelBuilder landscapeFilePath(String landscapeFilePath) {
    this.landscapeFilePath = landscapeFilePath;
    return this;
  }

  /**
   * Sets the file path of the file containing the initial hare density.
   *
   * @param hareDensityFilePath The file path.
   * @return                    The builder object
   */
  public PreyModelBuilder hareDensityFilePath(String hareDensityFilePath) {
    this.hareDensityFilePath = hareDensityFilePath;
    return this;
  }

  /**
   * Sets the file path of the file containing the initial puma density.
   *
   * @param pumaDensityFilePath The file path.
   * @return                    The builder object
   */
  public PreyModelBuilder pumaDensityFilePath(String pumaDensityFilePath) {
    this.pumaDensityFilePath = pumaDensityFilePath;
    return this;
  }

  /**
   * Sets the landscape..
   *
   * @param landscape   The landscape to be set.
   * @return            The builder object
   */
  public PreyModelBuilder landscape(Landscape landscape) {
    this.landscape = landscape;
    return this;
  }

  /**
   * Sets the hare density grid.
   *
   * @param h   The density grid to be set.
   * @return    The builder object
   */
  public PreyModelBuilder h(DensityGrid h) {
    this.h = h;
    return this;
  }

  /**
   * Sets the puma density grid.
   *
   * @param p   The density grid to be set.
   * @return    The builder object
   */
  public PreyModelBuilder p(DensityGrid p) {
    this.p = p;
    return this;
  }

  /**
   * Sets r - the hare birth rate.
   *
   * @param r   The value to be set.
   * @return    The builder object
   */
  public PreyModelBuilder r(double r) {
    this.r = r;
    return this;
  }

  /**
   * Sets a - the hare predation rate.
   *
   * @param a   The value to be set.
   * @return    The builder object
   */
  public PreyModelBuilder a(double a) {
    this.a = a;
    return this;
  }

  /**
   * Sets b - the puma birth rate per heare eaten.
   *
   * @param b   The value to be set.
   * @return    The builder object
   */
  public PreyModelBuilder b(double b) {
    this.b = b;
    return this;
  }

  /**
   * Sets m - the puma mortality rate.
   *
   * @param m   The value to be set.
   * @return    The builder object
   */
  public PreyModelBuilder m(double m) {
    this.m = m;
    return this;
  }

  /**
   * Sets k - the hare diffusion rate.
   *
   * @param k   The value to be set.
   * @return    The builder object
   */
  public PreyModelBuilder k(double k) {
    this.k = k;
    return this;
  }

  /**
   * Sets l - the puma diffusion rate.
   *
   * @param l   The value to be set.
   * @return    The builder object
   */
  public PreyModelBuilder l(double l) {
    this.l = l;
    return this;
  }

  /**
   * Sets the size of the time step.
   *
   * @param timeStepSize   The value to be set.
   * @return               The builder object
   */
  public PreyModelBuilder timeStepSize(double timeStepSize) {
    this.timeStepSize = timeStepSize;
    return this;
  }

  /**
   * Sets the number of time steps between successive output.
   *
   * @param numberOfTimeStepsBetweenOutput   The value to be set.
   * @return                                 The builder object
   */
  public PreyModelBuilder numberOfTimeStepsBetweenOutput(
    double numberOfTimeStepsBetweenOutput) {
    this.numberOfTimeStepsBetweenOutput = (int) numberOfTimeStepsBetweenOutput;
    return this;
  }

  /**
   * Sets the maximum time that the simulation runs for.
   *
   * @param maxTime   The value to be set.
   * @return          The builder object
   */
  public PreyModelBuilder maxTime(double maxTime) {
    this.maxTime = maxTime;
    return this;
  }

  /**
   * Sets the seed to used in the random initial condition of density.
   *
   * @param seed   The value to be set.
   * @return       The builder object
   */
  public PreyModelBuilder seed(double seed) {
    this.seed = (long) seed;
    return this;
  }

  /**
   * Gets the landscape.
   *
   * @return  The landscape
   */
  public Landscape getLandscape() {
    return landscape;
  }

  /**
   * Gets the hare density grid.
   *
   * @return  The hare density grid
   */
  public DensityGrid getH() {
    return h;
  }

  /**
   * Gets the puma density grid.
   *
   * @return  The puma density grid
   */
  public DensityGrid getP() {
    return p;
  }

  /**
   * Gets the file path of the file containing the landscape.
   *
   * @return  The landscape file path
   */
  public String getLandscapeFilePath() {
    return landscapeFilePath;
  }

  /**
   * Gets the file path of the file containing the initial hare density.
   *
   * @return  The initial hare density file path
   */
  public String getHareDensityFilePath() {
    return hareDensityFilePath;
  }

  /**
   * Gets the file path of the file containing the initial puma density.
   *
   * @return  The initial puma density file path
   */
  public String getPumaDensityFilePath() {
    return pumaDensityFilePath;
  }

  /**
   * Gets r - the hare birth rate.
   *
   * @return  the hare birth rate
   */
  public double getR() {
    return r;
  }

  /**
   * Gets a - the hare predation rate.
   *
   * @return  The the hare predation rate
   */
  public double getA() {
    return a;
  }

  /**
   * Gets b - the puma birth rate per hare eaten
   *
   * @return  The the puma birth rate per hare eaten
   */
  public double getB() {
    return b;
  }

  /**
   * Gets m - the puma mortality rate.
   *
   * @return  The puma mortality rate
   */
  public double getM() {
    return m;
  }

  /**
   * Gets k - the hare diffusion rate.
   *
   * @return  The hare diffusion rate
   */
  public double getK() {
    return k;
  }

  /**
   * Gets l - the puma diffusion rate.
   *
   * @return  The puma diffusion rate
   */
  public double getL() {
    return l;
  }

  /**
   * Gets the size of the time step.
   *
   * @return  The size of the time step
   */
  public double getTimeStepSize() {
    return timeStepSize;
  }

  /**
   * Gets the number of time steps between successive iterations.
   *
   * @return  The number of time steps between successive iterations
   */
  public double getNumberOfTimeStepsBetweenOutput() {
    return numberOfTimeStepsBetweenOutput;
  }

  /**
   * Gets the maximum time the simulation will run for.
   *
   * @return  The maximum time the simulation can run for
   */
  public double getMaxTime() {
    return maxTime;
  }

  /**
   * Overrides the to string method to provide a string based representation of the parameters.
   *
   * @return a string based representation of the parameters
   */
  @Override
  public String toString() {
    return "PreyModelBuilder{" + "parameterFilePath='"
      + parameterFilePath + '\'' + ", landscapeFilePath='"
      + landscapeFilePath + '\'' + ", hareDensityFilePath='"
      + hareDensityFilePath + '\'' + ", pumaDensityFilePath='"
      + pumaDensityFilePath + '\'' + ", r=" + r + ", a=" + a + ", b="
      + b + ", m=" + m + ", k=" + k + ", l=" + l + ", timeStepSize="
      + timeStepSize + ", numberOfTimeStepsBetweenOutput="
      + numberOfTimeStepsBetweenOutput + ", maxTime=" + maxTime + '}';
  }

}
