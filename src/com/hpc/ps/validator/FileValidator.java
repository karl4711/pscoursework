package com.hpc.ps.validator;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.io.File;

/**
 * File existence validator for JCommander
 * @author Bowen Sun Created on 23/10/2016.
 */
public class FileValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {

        File file = new File(value);
        if (!file.exists()) {
            throw new ParameterException("Parameter " + name + " should be a path to an existing file. (found " + value + ")");
        }
    }
}
