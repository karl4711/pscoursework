package com.hpc.ps.validator;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * Positive number validator for JCommander
 * @author Bowen Sun Created on 08/10/2016.
 */
public class PositiveValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {

        String validateMessage = "Parameter " + name + " should be a positive number(found " + value + ")";

        try {
            double n = Double.parseDouble(value);
            if (n < 0) {
                throw new ParameterException(validateMessage);
            }
        } catch (NumberFormatException e) {
            throw new ParameterException(validateMessage);
        }
    }
}
