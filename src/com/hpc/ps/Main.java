package com.hpc.ps;

import com.beust.jcommander.JCommander;
import com.hpc.ps.io.Input;
import com.hpc.ps.io.InputImpl;
import com.hpc.ps.model.PreyModel;
import com.hpc.ps.model.PreyModelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entrance of the program
 *
 * @author Bowen Sun, Nathan Giles-Donovan
 */
//Created on 07/10/2016.
public class Main {

  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {

    try {
      PreyModelBuilder builder = new PreyModelBuilder();
      JCommander j = new JCommander(builder, args);

      if(builder.help()){
        j.usage();
        return;
      }

      if (builder.getParameterFilePath() != null) {
        Input input = new InputImpl();
        input.readParameterConfig(builder.getParameterFilePath(), builder);

        new JCommander(builder, args);
      }
      logger.info("input model builder: " + builder.toString());

      PreyModel model = builder.buildModel();

      logger.info("Starting Now");

      long timeAtStart = System.currentTimeMillis();                    //Timer starts here
      model.start();
      long timeToRun = System.currentTimeMillis() - timeAtStart;        //Timer Ends Here
      logger.info("total time to run the model (hh:mm:ss.sss): " + formatRunTime(timeToRun));

    } catch (Exception e) {

      logger.error(e.getMessage(), e);
      logger.error("Fatal Error. Program will now close.");
      System.exit(1);
    }
  }

    /**
     * Formats the runtime.
     *
     * @param runTimeLong runtime in ms
     * @return readable string of time
     */
    public static String formatRunTime(long runTimeLong) {
    double runTime = runTimeLong/1000d;
    int hours = (int)runTime / 3600;
    int minutes = (int)(runTime % 3600) / 60;
    double seconds = runTime % 60;
    return String.format("%02d:%02d:%06.3f", hours, minutes, seconds);
  }
}
