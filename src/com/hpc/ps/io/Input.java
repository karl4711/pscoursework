package com.hpc.ps.io;

import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.Landscape;
import com.hpc.ps.model.PreyModelBuilder;

import java.io.IOException;

/**
 * Interface for inputting parameters
 *
 * @author Bowen Sun, Jiangsu Du Created on 10/10/2016.
 */
public interface Input {

	/**
	 * Read the parameters configuration xml file and put the parameters into
	 * the model builder
	 * 
	 * @param paramConfigPath
	 *            xml file path
	 * @param builder
	 *            the model builder
	 * @throws Exception
	 */
	void readParameterConfig(String paramConfigPath, PreyModelBuilder builder)
			throws Exception;

	/**
     * Read the landscape from specified file
     *
     * @param landscapeFilePath specified landscape file path
     * @return landcape
     * @throws IOException
     */
    Landscape readLandscape(String landscapeFilePath) throws IOException;

    /**
     * Read the density grid from specified file
     *
     * @param densityGridFilePath specified density file path
     * @return density grid
     * @throws IOException
     */
    DensityGrid readDensityGrid(String densityGridFilePath) throws IOException;

}
