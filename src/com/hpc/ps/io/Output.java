package com.hpc.ps.io;

import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.Landscape;

import java.io.IOException;
import java.util.List;

/**
 * the output interface
 * @author Bowen Sun, Dimitrios Stefanos Velissariou Created on 10/10/2016.
 */
public interface Output {

    /**
     * output the Plain PPM file
     *
     * @param h The density grid of hares.
     * @param p The density grid of pumas.
     * @param l The landscape grid.
     * @return The output file name
     */
    String writePPM(DensityGrid h, DensityGrid p, Landscape l);

    /**
     * output a list of average values in the output.csv file
     *
     * @param hareAverageList A list of average values of hares across the grid.
     * @param pumaAverageList A list of average values of pumas across the grid.
     * @return The output file name
     * @throws IOException 
     */
    String writeAverages(List<Double> hareAverageList, List<Double> pumaAverageList) throws IOException;
}
