package com.hpc.ps.io;

import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.DensityGridImpl;
import com.hpc.ps.model.Landscape;
import com.hpc.ps.model.PreyModelBuilder;
import com.hpc.ps.validator.FileValidator;
import com.hpc.ps.validator.PositiveValidator;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * The input implementation
 * @author Jiangsu Du Created on 10/10/2016.
 */
public class InputImpl implements Input {

	
	
	public void readParameterConfig(String paramConfigPath,
			PreyModelBuilder builder) throws Exception {

		PositiveValidator positiveValidator = new PositiveValidator();
		FileValidator fileValidator =  new FileValidator();
		
		File file = new File(paramConfigPath);

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory
				.newDocumentBuilder();
		Document document = documentBuilder.parse(file);

		NodeList nodeList = document.getElementsByTagName("Parameters").item(0)
				.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node currentNode = nodeList.item(i);

			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {

				try {
					double value = Double.parseDouble(currentNode
							.getTextContent());
					positiveValidator.validate(currentNode.getNodeName(), currentNode.getTextContent());
					PreyModelBuilder.class.getMethod(currentNode.getNodeName(),
							double.class).invoke(builder, value);

				} catch (NumberFormatException e) {
					fileValidator.validate(currentNode.getNodeName(), currentNode.getTextContent());
					PreyModelBuilder.class.getMethod(currentNode.getNodeName(),
							String.class).invoke(builder,
							currentNode.getTextContent());
				} catch (NoSuchMethodException e2) {
					throw new IllegalArgumentException(
							"Error in configuration file. No such parameter as: "
									+ currentNode.getNodeName());
				}
			}
		}
	}


	public DensityGrid readDensityGrid(String path) throws IOException {
		
		//Open the File
		File file = new File(path);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			line = br.readLine();
			
			//get the number of columns and rows from the first line and initialize
			int numberOfCols = Integer.parseInt(line.split(" ")[0]);
			int numberOfRows = Integer.parseInt(line.split(" ")[1]);
			DensityGrid dg = new DensityGridImpl(numberOfCols, numberOfRows);
			
			//read density in
			for (int i = 0; i < numberOfCols; i++) {
				line = br.readLine();
				if (line != null) {
					String[] lineArr = line.split(" ");
					for (int j = 0; j < numberOfRows; j++) {
						dg.set(i, j, Double.parseDouble(lineArr[j]));
					}
				}
			}
			return dg;
		} finally {
			if (br != null) {
				br.close();
			}
		}

	}

   
	public Landscape readLandscape(String landscapeFilePath) throws IOException {

		Landscape landscape;
		
		//open the file
		File file = new File(landscapeFilePath);
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			
			//read first line and initialize landscape
			line = br.readLine();
			int numberOfCols = Integer.parseInt(line.split(" ")[0]);
			int numberOfRows = Integer.parseInt(line.split(" ")[1]);
			landscape = new Landscape(numberOfRows, numberOfCols);

			//read each line and assign value 
			for (int i = 0; i < numberOfRows; i++) {
				line = br.readLine();
				if (line != null) {
					String[] lineArray = line.split(" ");
					for (int j = 0; j < lineArray.length; j++) {
						landscape.setLandscapeElement(i, j,
								Integer.parseInt(lineArray[j]));
					}
				}
			}

		} finally {
			if (br != null) {
				br.close();
			}
		}
		return landscape;
	}

}
