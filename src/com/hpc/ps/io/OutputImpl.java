package com.hpc.ps.io;

// Used for file input/output

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

// Used for the dates
import java.text.SimpleDateFormat;
import java.util.Date;

// Used for the grids
import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.Landscape;

/**
 * The output implementation
 *
 * @author Dimitrios Stefanos Velissariou
 */
public class OutputImpl implements Output {

  private int fileNumber = 0;
  private boolean isFirstTime = true;
  private String dateTimeString = "";
	private final String basePath;

	public OutputImpl(){
		basePath = "./files";
	}

	public OutputImpl(String basePath){
		this.basePath = basePath;
	}

  public String writePPM(DensityGrid hareGrid, DensityGrid pumaGrid,
                         Landscape landscape) {
    /*
		 * Hare density of a cell will be represented by the green value in the
		 * RGB of the PPM image, pumas will be represented by the red value of
		 * the RGB in the PPM image:
		 */

		// Get the grid dimensions:
		int columns = hareGrid.getColumnsNumber();
		int rows = hareGrid.getRowsNumber();

		// Find the current time in order to timestamp the folder of the output heatmaps:
		setDateTimeString();

		// Find the number of the PPM file:
		String pathString = basePath+"/heatmaps/".concat(dateTimeString).concat("/");

		File path = new File(pathString);

		// Create the directory of the heat maps if it does not exist
		if (!path.exists()) {
			path.mkdirs();
		}

		// Increment the file number:
		fileNumber++;

		// Create the PPM file name:
		String ppmFileName = pathString + "heatmap" + fileNumber + ".ppm";

		try (FileWriter fw = new FileWriter(ppmFileName, false);
				BufferedWriter bw = new BufferedWriter(fw)) {
			
			// Find the maximum density value in the two grids
			double max = 0;
			double maxPumaDensity = maxArrayValue(pumaGrid);
			double maxHareDensity = maxArrayValue(hareGrid);
			if (maxPumaDensity < maxHareDensity) {
				max = maxHareDensity;
			} else {
				max = maxPumaDensity;
			}
			
			// Write the type of the PPM file
			bw.append("P3");
			bw.append(System.lineSeparator());
			// Write a comment that explains the file
			bw.append("#Predator-Prey model heatmap. Maximum density value: ");
			bw.append(String.valueOf(max));
			bw.append(System.lineSeparator());
			// Write the dimensions of the grid
			bw.append(String.valueOf(columns));
			bw.append(" ");
			bw.append(String.valueOf(rows));
			bw.append(System.lineSeparator());
			// Set the number of colors to 256
			bw.append("255".concat(System.lineSeparator()));
			// Write the values of the PPM file

			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < columns ; j++) {
					// Write the Red, Green and Blue value of a pixel/cell:
					bw.append(String.valueOf(Math.round(pumaGrid.get(i, j) / max * 255)));
					bw.append(" ");
					bw.append(String.valueOf(Math.round(hareGrid.get(i, j) / max * 255)));
					bw.append(" ");
					bw.append(String.valueOf((1 - landscape.getLandscapeElement(i, j)) * 255));
					// The new lines i the PPM file are every 5 pixels
					if ((((j + 1)+(i*rows)) % 5) == 0) {
						bw.append(System.lineSeparator());
					} else {
						bw.append("  ");
					}
				} // End columns for loop
			} // End rows for loop
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ppmFileName;
	}
	
	
	public String writeAverages(List<Double> hareAverageList,
			List<Double> pumaAverageList) throws IOException {
		setDateTimeString();
		
		// Name of the output file:
		String outputFileName = basePath + "/output"+dateTimeString+".csv";

		// Check if it is the first time writing to the output CSV file.
		if (!isFirstTime) {
			isFirstTime = false;
		}

		// Create or append the output file
		// Appends if it is not the first time:
		try (FileWriter fw = new FileWriter(outputFileName, !isFirstTime);
				BufferedWriter bw = new BufferedWriter(fw)) {
			// Write headers:
			bw.append("Hare Density, Puma Density");
			bw.append(System.lineSeparator());

			// Write the average values to the output CSV file:
			int n = hareAverageList.size();
			for (int i = 0; i < n; i++) {
				bw.append(String.valueOf(hareAverageList.get(i)));
				bw.append(",");
				bw.append(String.valueOf(pumaAverageList.get(i)));
				bw.append(System.lineSeparator());
			}
		}
		return outputFileName;
	}


    /**
     * This method returns the maximum density value of a grid
      * @param grid
     * @return
     */
	private double maxArrayValue(DensityGrid grid) {
		// Get the size of the grid:
		int numberOfRows = grid.getRowsNumber();
		int numberOfColumns = grid.getColumnsNumber();

		// Find the maximum value:
		double max = 0;
		for (int row = 0; row < numberOfRows; row++) {
			for (int column = 0; column < numberOfColumns; column++) {
				if (max < grid.get(row, column)) {
					max = grid.get(row, column);
				}
			}
		}
		return max;
	}


    /**
     * This method set the timestamp
     */
	private void setDateTimeString(){
		if (dateTimeString.equals("")) {
			Date dateTime = new Date();
      SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
			dateTimeString = myFormatter.format(dateTime);
		}
	}
}
