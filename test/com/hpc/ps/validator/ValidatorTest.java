package com.hpc.ps.validator;

import com.beust.jcommander.ParameterException;
import com.hpc.ps.validator.FileValidator;
import com.hpc.ps.validator.PositiveValidator;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for param validators
 *
 * @author Bowen Sun
 */
public class ValidatorTest {

    @Test
    public void testPositiveValidator() {

        String name = "testParam";

        // test positive number
        String value = "1.0";
        PositiveValidator validator = new PositiveValidator();
        try {
            validator.validate(name, value);
        } catch (ParameterException e) {
            Assert.fail("Validation should pass while value is a positive number.");
        }

        // test negative number
        value = "-1.0";
        boolean flag = true;
        try {
            validator.validate(name, value);
        } catch (ParameterException e) {
            flag = false;
        }
        if (flag) {
            Assert.fail("Validation should not pass while value is a negative number.");
        }

        // test a string
        value = "testValue";
        flag = true;
        try {
            validator.validate(name, value);
        } catch (ParameterException e) {
            flag = false;
        }
        if (flag) {
            Assert.fail("Validation should not pass while value is not a number.");
        }

    }

    @Test
    public void testFileValidator() {

        String name = "testParam";
        String value = "files/parameter.xml";
        FileValidator validator = new FileValidator();

        try {
            validator.validate(name, value);
        } catch (ParameterException e) {
            Assert.fail("Validation should pass while value is an existing file path.");
        }

        value = "testValue";
        try {
            validator.validate(name, value);
        } catch (ParameterException e) {
            return;
        }
        Assert.fail("Validation should not pass while value is not an existing file path.");
    }
}
