package com.hpc.ps.model.solver;

import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.DensityGridImpl;
import com.hpc.ps.model.DensityType;
import com.hpc.ps.model.solver.Solver;
import com.hpc.ps.model.solver.SolverParameters;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Nathan on 21/10/2016.
 */
public class SolverTest {
	private final static double tolerance = 0.001;

	private DensityGrid testHaresGrid;
	private DensityGrid testPumasGrid;

	@Test
	public void TestHaresUpdate() {
		Solver solver = new Solver(new SolverParameters(0.08, 0.04, 0.02, 0.06,
				0.2, 0.2), 0.4);

		initialise();

		double newVal = solver.updateDensity(DensityType.HARES, 4, 6,
				testHaresGrid, testPumasGrid, 4);
		testHaresGrid.set(4, 6, newVal);
		Assert.assertEquals("Value of updated hare density is unexpected",
				3.744, testHaresGrid.get(4, 6), tolerance);
	}

	@Test
	public void TestPumasUpdate() {
		Solver solver = new Solver(new SolverParameters(0.08, 0.04, 0.02, 0.06,
				0.2, 0.2), 0.4);

		initialise();

		double newVal = solver.updateDensity(DensityType.PUMAS, 4, 6,
				testHaresGrid, testPumasGrid, 4);
		testPumasGrid.set(4, 6, newVal);
		Assert.assertEquals("Value of updated hare density is unexpected",
				6.048, testPumasGrid.get(4, 6), tolerance);
	}

	@Test
	public void TestNegativeHaresUpdate() {
		Solver solver = new Solver(new SolverParameters(0.08, 4, 0.02, 0.06,
				0.2, 0.2), 0.4);

		initialise();

		double newVal = solver.updateDensity(DensityType.HARES, 4, 6,
				testHaresGrid, testPumasGrid, 4);
		testHaresGrid.set(4, 6, newVal);
		Assert.assertEquals("Value of updated hare density is unexpected", 0,
				testHaresGrid.get(4, 6), tolerance);
	}

	@Test
	public void TestNegativePumasUpdate() {
		Solver solver = new Solver(new SolverParameters(0.08, 0.04, 0.02, 6,
				0.2, 0.2), 0.4);

		initialise();

		double newVal = solver.updateDensity(DensityType.PUMAS, 4, 6,
				testHaresGrid, testPumasGrid, 4);
		testPumasGrid.set(4, 6, newVal);
		Assert.assertEquals("Value of updated hare density is unexpected", 0,
				testPumasGrid.get(4, 6), tolerance);
	}

	private void initialise(){
		testHaresGrid = new DensityGridImpl(10, 10);
		for (int i = 0; i < testHaresGrid.getRowsNumber(); i++) {
			for (int j = 0; j < testHaresGrid.getColumnsNumber(); j++) {
				testHaresGrid.set(i, j, i);
			}
		}

		testPumasGrid = new DensityGridImpl(10, 10);
		for (int i = 0; i < testPumasGrid.getRowsNumber(); i++) {
			for (int j = 0; j < testPumasGrid.getColumnsNumber(); j++) {
				testPumasGrid.set(i, j, j);
			}
		}
	}


}
