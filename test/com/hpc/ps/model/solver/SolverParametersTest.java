package com.hpc.ps.model.solver;

import com.hpc.ps.model.solver.SolverParameters;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Nathan on 21/10/2016.
 */
public class SolverParametersTest {
  final static double tolerance = 0.01;

  @Test
  public void testGetR(){
    double testR = 0.01;
    SolverParameters solverParameters = new SolverParameters(testR,0,0,0,0,0);
    Assert.assertEquals("Value of getR() unexpected", testR, solverParameters.getR(), tolerance);
  }

  @Test
  public void testGetA(){
    double testA = 0.01;
    SolverParameters solverParameters = new SolverParameters(0,testA,0,0,0,0);
    Assert.assertEquals("Value of getA() unexpected", testA, solverParameters.getA(), tolerance);
  }

  @Test
  public void testGetB(){
    double testB = 0.01;
    SolverParameters solverParameters = new SolverParameters(0,0,testB,0,0,0);
    Assert.assertEquals("Value of getB() unexpected", testB, solverParameters.getR(), tolerance);
  }

  @Test
  public void testGetM(){
    double testM = 0.01;
    SolverParameters solverParameters = new SolverParameters(0,0,0,testM,0,0);
    Assert.assertEquals("Value of getM() unexpected", testM, solverParameters.getM(), tolerance);
  }

  @Test
  public void testGetK(){
    double testK = 0.01;
    SolverParameters solverParameters = new SolverParameters(0,0,0,0,testK,0);
    Assert.assertEquals("Value of getK() unexpected", testK, solverParameters.getK(), tolerance);
  }

  @Test
  public void testGetL(){
    double testL = 0.01;
    SolverParameters solverParameters = new SolverParameters(0,0,0,0,0,testL);
    Assert.assertEquals("Value of getL() unexpected", testL, solverParameters.getL(), tolerance);
  }
}
