package com.hpc.ps.model;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * Created by Nathan on 31/10/2016.
 */
public class LandscapeTest {

  @Test
  public void GetElementTest(){
    Landscape testLandscape = new Landscape(10,8);
    try {
      testLandscape.setLandscapeElement(7,7,Landscape.LAND);
    }catch (Exception e){
      fail("Set Landscape element failed");
    }
    Assert.assertEquals("GetLandscapeElement return value was unexpected", Landscape.LAND, testLandscape.getLandscapeElement(7,7));
  }

  @Test
  public void getNumberOfRowsTest(){
    Landscape testLandscape = new Landscape(10,8);
    Assert.assertEquals("Return value was unexpected", 10, testLandscape.getNumberOfRows());
  }

  @Test
  public void getNumberOfColsTest(){
    Landscape testLandscape = new Landscape(10,8);
    Assert.assertEquals("Return value was unexpected", 8, testLandscape.getNumberOfCols());
  }

  @Test
  public void getNumberOfSitesNotWaterTest(){
    Landscape testLandscape = new Landscape(10,8);
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 8; j++) {
        testLandscape.setLandscapeElement(i,j,Landscape.LAND);
      }
    }

    //set one to be water
    testLandscape.setLandscapeElement(5,6,Landscape.WATER);

    Assert.assertEquals("Return value was unexpected", 79, testLandscape.getNumberOfSiteNotWater());
  }

  @Test
  public void getNumberOfNeighboursNotWaterTest(){
    Landscape testLandscape = new Landscape(10,8);

    /* Set the landscape to be like:
     *  1,0,1,0,1,0,...
     *  1,0,1,0,1,0....
     *  .
     *  .
     *  .
     */
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 8; j+=2) {
        testLandscape.setLandscapeElement(i,j,Landscape.LAND);
      }
    }

    Assert.assertEquals("Return value was unexpected", 2, testLandscape.getNumberOfNeighboursNotWater(4,4));
  }

  // Test the boundary case of the landscape - the outer sites of the landscape will have less
  // neighbours. The boundary condition is adding a halo of water.
  @Test
  public void getNumberOfNeighboursNotWaterBoundaryTest(){
    Landscape testLandscape = new Landscape(10,8);

    /* Set the landscape to be:
     *  1,0,1,0,1,0,...
     *  1,0,1,0,1,0....
     *  .
     *  .
     *  .
     */
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 8; j+=2) {
        testLandscape.setLandscapeElement(i,j,Landscape.LAND);
      }
    }

    Assert.assertEquals("Return value was unexpected", 2, testLandscape.getNumberOfNeighboursNotWater(1,0));
  }


}
