package com.hpc.ps.model;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Nathan on 31/10/2016.
 */
public class DensityGridImplTest {
  private final static double tolerance = 0.001;

  @Test
  public void testBoundaryConditions(){
    DensityGrid testGrid = new DensityGridImpl(10,8);

    Assert.assertEquals("Value returned was unexpected",0,testGrid.get(2,-1),tolerance);
    Assert.assertEquals("Value returned was unexpected",0,testGrid.get(-1,0),tolerance);
    Assert.assertEquals("Value returned was unexpected",0,testGrid.get(5,9),tolerance);
    Assert.assertEquals("Value returned was unexpected",0,testGrid.get(11,4),tolerance);
  }
}
