package com.hpc.ps.model;

import com.hpc.ps.model.PreyModelBuilder;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class PreyModelBuilderTest {
  final double epsilon = 0.0001d;

  @Test
  public void buildModelRandomUnseededTest() throws IOException {
    PreyModelBuilder builder = new PreyModelBuilder();
    String landscapeFilePath = "files/small.dat";
    builder.landscapeFilePath(landscapeFilePath);
    builder.buildModel();
    double densityTest = builder.getH().get(10, 19);
    builder.buildModel();

    //For unseeded, initial conditions should not be the same
    Assert.assertNotSame("Two runs produced the same random results", builder.getH().get(10, 19), densityTest);

    //The two populations are also not identical
    Assert.assertNotSame("The two populations are the same", builder.getH().get(10, 19), builder.getP().get(10, 19));
  }

  @Test
  public void buildModelRandomSeededTest() throws IOException {
    PreyModelBuilder builder = new PreyModelBuilder();
    builder.seed(12345);
    String landscapeFilePath = "files/small.dat";
    builder.landscapeFilePath(landscapeFilePath);
    builder.buildModel();
    double densityTest = builder.getH().get(10, 19);
    builder.buildModel();

    //Ensures that for seeded, initial conditions are reproducible
    Assert.assertEquals("Two runs did not reproduce random results", builder.getH().get(10, 19), densityTest, epsilon);

    //But that the two populations are not identical
    Assert.assertNotSame("The two populations are the same", builder.getH().get(10, 19), builder.getP().get(10, 19));
  }

  @Test
  public void buildModelFileInputTest() throws IOException {
    PreyModelBuilder builder = new PreyModelBuilder();
    String densityGridFilePath = "files/densitytest.dat";
    builder.hareDensityFilePath(densityGridFilePath);
    builder.pumaDensityFilePath(densityGridFilePath);
    builder.buildModel();
    Assert.assertEquals(builder.getH().get(10, 19), 39, epsilon);
    Assert.assertEquals(builder.getP().get(10, 19), 39, epsilon);
  }

  @Test
  public void toStringTest(){
    PreyModelBuilder builder = new PreyModelBuilder();
      String expectedString = "PreyModelBuilder{parameterFilePath='null', " +
        "landscapeFilePath='files/islands.dat', hareDensityFilePath='null', "
        + "pumaDensityFilePath='null', r=0.08, a=0.04, b=0.02, m=0.06, k=0.2, l=0.2, timeStepSize="
        + "0.4, numberOfTimeStepsBetweenOutput=10, maxTime=500.0}";
    Assert.assertEquals("Returned value is unexpected", expectedString, builder.toString());
  }
}
