package com.hpc.ps.model;

import com.hpc.ps.io.Input;
import com.hpc.ps.io.InputImpl;
import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.DensityGridImpl;
import com.hpc.ps.model.Landscape;
import com.hpc.ps.model.PreyModel;
import com.hpc.ps.model.solver.SolverParameters;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

import static org.junit.Assert.fail;

/**
 * Created by Nathan on 21/10/2016.
 */
public class PreyModelTest {

  private final static double tolerance = 0.001;

  private final String densityGridFilePath = "./files/densitytest.dat";
  private String landscapeFilePath = "./files/small.dat";
  private Input input = new InputImpl();
  private DensityGrid testHareDensity;
  private DensityGrid testPumaDensity;
  private Landscape testLandscape;
  private final SolverParameters testSolverParams = new SolverParameters(0.08, 0.04, 0.02, 0.06, 0.2, 0.2);
  private PreyModel testModel;

  @Test
  public void calcAverageHareDensityTest() {
    Assert.assertEquals("The value returned is unexpected", 50.99061033, testModel.calcAverageHareDensity(), tolerance);
  }

  @Test
  public void calcAveragePumaDensityTest() {
    Assert.assertEquals("The value returned is unexpected", 50.99061033, testModel.calcAveragePumaDensity(), tolerance);
  }

  @Test
  public void testGrid(){
    landscapeFilePath = "./files/small.dat";
    try {
      testLandscape = input.readLandscape(landscapeFilePath);
    } catch (IOException e) {
      fail("Loading of landscape threw an IOException: " + e.getMessage());
      return;
    }
    Random rand = new Random(12345);
    testHareDensity = new DensityGridImpl(testLandscape, rand, 5);
    testPumaDensity = new DensityGridImpl(testLandscape, rand, 5);
    testModel = new PreyModel(testLandscape, testHareDensity, testPumaDensity, testSolverParams, 0.4, 1, 50, "./testOutput");
    try{
      testModel.start();
    }catch (IOException e){
      fail("Running the com.hpc.ps.model caused an IOException: " + e.getMessage());
    }
  }

  @Before
  public void initialise() {
    try {
      testHareDensity = input.readDensityGrid(densityGridFilePath);
      testPumaDensity = input.readDensityGrid(densityGridFilePath);
      testLandscape = input.readLandscape(landscapeFilePath);
    } catch (IOException e) {
      fail("Loading of test files threw an IOException: " + e.getMessage());
      return;
    }
    testModel = new PreyModel(testLandscape, testHareDensity, testPumaDensity, testSolverParams, 0.4, 1, 500, "./testOutput");
  }

  @After
  public void cleanUpTestOutputs(){
    File path = new File("./testOutput");
    if(path.exists()) {
      try {
        delete(path);
      }catch (IOException e){
        //Do nothing?
      }
    }
  }

  private void delete(File f) throws IOException {
    if (f.isDirectory()) {
      for (File c : f.listFiles())
        delete(c);
    }
    if (!f.delete())
      throw new FileNotFoundException("Failed to delete file: " + f);
  }
}
