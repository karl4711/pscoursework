package com.hpc.ps;

import com.hpc.ps.Main;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Nathan on 27/10/2016.
 */
public class MainTest {

  @Test
  public void formatRunTimeTest(){
    long runtime = 7621212;
    Assert.assertEquals("Returned String is unexpected", "02:07:01.212", Main.formatRunTime(runtime));
  }

}
