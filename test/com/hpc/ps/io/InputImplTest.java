package com.hpc.ps.io;

import com.hpc.ps.io.Input;
import com.hpc.ps.io.InputImpl;
import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.Landscape;
import com.hpc.ps.model.PreyModelBuilder;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for input implementation
 *
 * @author Bowen Sun, Jiangsu Du
 */
public class InputImplTest {

    private Input input = new InputImpl();
    private final double epsilon = 0.0001d;

  @Test
  public void testReadParameterConfig() throws Exception {
    String paramConfigPath = "files/parameter.xml";

    PreyModelBuilder builder = new PreyModelBuilder();

    //test function readParameterConfig
    input.readParameterConfig(paramConfigPath, builder);

    Assert.assertEquals(builder.getLandscapeFilePath(), "files/islands.dat");
    Assert.assertEquals(builder.getHareDensityFilePath(), "files/densitytest.dat");
    Assert.assertEquals(builder.getPumaDensityFilePath(), "files/densitytest.dat");

    Assert.assertEquals(builder.getR(), 0.1d, epsilon);
    Assert.assertEquals(builder.getA(), 0.05d, epsilon);
    Assert.assertEquals(builder.getB(), 0.03d, epsilon);
    Assert.assertEquals(builder.getM(), 0.05d, epsilon);
    Assert.assertEquals(builder.getK(), 0.3d, epsilon);
    Assert.assertEquals(builder.getL(), 0.3d, epsilon);
    Assert.assertEquals(builder.getTimeStepSize(), 0.4d, epsilon);
    Assert.assertEquals(builder.getNumberOfTimeStepsBetweenOutput(), 10, epsilon);
    Assert.assertEquals(builder.getMaxTime(), 500, epsilon);

  }

  @Test
  public void testReadLandscape() throws Exception {
    String landscapeFilePath = "files/small.dat";
    Landscape ld = input.readLandscape(landscapeFilePath);
    Assert.assertEquals(ld.getNumberOfCols(), 50, epsilon);
    Assert.assertEquals(ld.getNumberOfRows(), 50, epsilon);
    Assert.assertEquals(ld.getLandscapeElement(10, 19), 1, epsilon);

  }

  @Test
  public void testReadDensity() throws Exception {
    String densityGridFilePath = "files/densitytest.dat";
    DensityGrid dg = input.readDensityGrid(densityGridFilePath);

    Assert.assertEquals(dg.getRowsNumber(), 50, epsilon);
    Assert.assertEquals(dg.getColumnsNumber(), 50, epsilon);
    Assert.assertEquals(dg.get(10,19), 39, epsilon);
  }

}
