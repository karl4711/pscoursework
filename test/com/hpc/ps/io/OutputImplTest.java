package com.hpc.ps.io;

import com.hpc.ps.io.OutputImpl;
import com.hpc.ps.model.DensityGrid;
import com.hpc.ps.model.DensityGridImpl;
import com.hpc.ps.model.Landscape;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * The output implementation JUnit test
 *
 * @author Dimitrios Stefanos Velissariou
 */
public class OutputImplTest {

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  /**
   * This method tests that the writePPM method creates the PPM file correctly.
   */
  @Test
  public void testWritePPM() throws IOException {
    // Create the expected output file:
    File expected = folder.newFile("test_heatmap.txt");
    FileWriter fw = new FileWriter(expected);
    BufferedWriter bf = new BufferedWriter(fw);
    bf.append("P3")
      .append(System.lineSeparator())
      .append("#Predator-Prey model heatmap. Maximum density value: 255.0")
      .append(System.lineSeparator())
      .append("1 6")
      .append(System.lineSeparator())
      .append("255")
      .append(System.lineSeparator())
      .append("255 254 0  0 0 255  100 99 0  50 50 0  25 25 0")
      .append(System.lineSeparator())
      .append("0 0 255  ");
    bf.close();
    fw.close();

    // Create the input and run the method:
    final File output;
    OutputImpl myOutput = new OutputImpl();

    // Sample test data
    DensityGrid h = new DensityGridImpl(6, 1);
    h.set(0, 0, 254);
    h.set(1, 0, 0);
    h.set(2, 0, 99);
    h.set(3, 0, 50);
    h.set(4, 0, 25);
    h.set(5, 0, 0);

    DensityGrid p = new DensityGridImpl(6, 1);
    p.set(0, 0, 255);
    p.set(1, 0, 0);
    p.set(2, 0, 100);
    p.set(3, 0, 50);
    p.set(4, 0, 25);
    p.set(5, 0, 0);

    Landscape l = new Landscape(6, 1);
    l.setLandscapeElement(0, 0, 1);
    l.setLandscapeElement(1, 0, 0);
    l.setLandscapeElement(2, 0, 1);
    l.setLandscapeElement(3, 0, 1);
    l.setLandscapeElement(4, 0, 1);
    l.setLandscapeElement(5, 0, 0);

    output = new File(myOutput.writePPM(h, p, l));

    // Load the two files in memory:
    byte[] expectedFileBytes = Files.readAllBytes(Paths.get(expected.getAbsolutePath()));
    byte[] outputFileBytes = Files.readAllBytes(Paths.get(output.getAbsolutePath()));

    // Convert read files to strings:
    String expectedFileString = new String(expectedFileBytes, StandardCharsets.UTF_8);
    String outputFileString = new String(outputFileBytes, StandardCharsets.UTF_8);

    // Compare the resulting output with the expected output:
    Assert.assertEquals("The averages output is not as expected.",
      expectedFileString, outputFileString);
  }


  /**
   * This method tests that the writePPM method can create multiple PPM files correctly.
   */
  @Test
  public void testMultipleWritePPM() throws IOException {
    OutputImpl myOutput = new OutputImpl();

    int dimension = 8;
    DensityGrid hareGrid = new DensityGridImpl(dimension, dimension);
    DensityGrid pumaGrid = new DensityGridImpl(dimension, dimension);
    Landscape landGrid = new Landscape(dimension, dimension);

    // Generating random numbers for the hare and puma density values:
    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        hareGrid.set(i, j, Math.random() * 100);
        pumaGrid.set(i, j, Math.random() * 100);
        landGrid.setLandscapeElement(i, j, 1);
      }
    }

    // Write ten ppm files:
    String output = null;
    for (int i = 0; i < 10; i++) {
      output = myOutput.writePPM(hareGrid, pumaGrid, landGrid);
    }

    // Count the number of heatmap files:
    File file = new File(output);
    output = file.getAbsoluteFile().getParent();
    int counter = 0;
    File fileName = new File(output + File.separator + "heatmap1.ppm");
    while (fileName.exists()) {
      counter++;
      fileName = new File(output + File.separator + "heatmap" + (counter + 1) + ".ppm");
    }

    // Compare the number of the files created with the number of files that
    // should have been created (10 files).
    Assert.assertEquals("The number of the outputs is incorrect.", 10, counter);
  }


  /**
   * This method tests that the writeAverages method creates the CSV file correctly.
   */
  @Test
  public void testWriteAverages() throws IOException, NoSuchAlgorithmException {

    // Create the expected output file:
    File expected = folder.newFile("test_heatmap.txt");
    FileWriter fw = new FileWriter(expected);
    BufferedWriter bf = new BufferedWriter(fw);
    //Write expected headers:
    bf.append("Hare Density, Puma Density");
    bf.append(System.lineSeparator());
    // Write expected average values:
    bf.append("100.0,0.8")
      .append(System.lineSeparator())
      .append("253.0,0.9")
      .append(System.lineSeparator());
    bf.close();
    fw.close();

    // Create the input and run the method:
    final File output;

    OutputImpl myOutput = new OutputImpl();

    // Sample test data:
    List<Double> hareList = new ArrayList<Double>();
    hareList.add(100.0);
    hareList.add(253.0);
    List<Double> pumaList = new ArrayList<Double>();
    pumaList.add(0.8);
    pumaList.add(0.9);
    output = new File(myOutput.writeAverages(hareList, pumaList));

    // Load the two files in memory:
    byte[] expectedFileBytes = Files.readAllBytes(Paths.get(expected.getAbsolutePath()));
    byte[] outputFileBytes = Files.readAllBytes(Paths.get(output.getAbsolutePath()));

    // Convert read files to strings:
    String expectedFileString = new String(expectedFileBytes, StandardCharsets.UTF_8);
    String outputFileString = new String(outputFileBytes, StandardCharsets.UTF_8);

    // Compare the resulting output with the expected output:
    Assert.assertEquals("The averages output is not as expected.",
      expectedFileString, outputFileString);
  }
}
